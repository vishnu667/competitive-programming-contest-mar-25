### Problem Statement 19

Given a non-negative integer number in base 10, find out how many 1s it contains when you express in Binary as an unsigned Integer.


**Input Format**

The first line of input consists of an integer T. This is the number of test cases. Then T lines follow with the input format as follows:

N


Here N is an integer.


**Constraints**


1 <= T <= 10000

0 <= N <= 65535

**Sample Input**
```
1
12
```

**Sample Output**
```
2
```
